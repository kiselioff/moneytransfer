package db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.embed.postgresql.EmbeddedPostgres;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static ru.yandex.qatools.embed.postgresql.distribution.Version.Main.V9_6;

public class DBConnection {

    private static Logger logger = LoggerFactory.getLogger(DBConnection.class);
    private static Connection connection;

    private DBConnection() {
    }

    private static Connection init() {
        try {
            Class.forName("org.postgresql.Driver");
            final EmbeddedPostgres postgres = new EmbeddedPostgres(V9_6);
            final String url = postgres.start("localhost", 5432, "dbName", "userName", "password");
            connection = DriverManager.getConnection(url);
            logger.info("Connected to DataBase");
        } catch (ClassNotFoundException e) {
            logger.info(String.format("Can't get driver: %s", e.getMessage()));
            connection = null;
        } catch (SQLException e) {
            logger.info(String.format("DB connection is unsuccessfull: %s", e.getMessage()));
            connection = null;
        } catch (IOException e) {
            logger.error(String.format("DB not started: %s", e.getMessage()));
            connection = null;
        }
        return connection;
    }

    public static Connection getConnection() {
        if (connection == null) {
            init();
        }
        return connection;
    }
}
