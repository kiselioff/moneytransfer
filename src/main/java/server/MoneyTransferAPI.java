package server;

import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import exceptions.TransferException;
import model.AccountPOJO;
import model.DAO;
import model.TransferPOJO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.MoneyTransferService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("api")
@Produces(MediaType.APPLICATION_JSON)
public class MoneyTransferAPI {

    private static Logger logger = LoggerFactory.getLogger(MoneyTransferAPI.class);

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getHello() {
        String greeting = "service is running";
        logger.info(greeting);
        return greeting;
    }

    @GET
    @Path("/balances")
    @Produces(MediaType.APPLICATION_JSON)
    public JSONWrappedObject getAccounts() {
        List<AccountPOJO> accounts = DAO.getAccounts();
        return new JSONWrappedObject(null, null, accounts);
    }

    @PUT
    @Path("/transfer")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response transferMoney(TransferPOJO transferPOJO) {
        MoneyTransferService mts = new MoneyTransferService();
        String errorMsg = "\"Your transfer failed\"";
        try {
            mts.transfer(transferPOJO);
        } catch (TransferException e) {
            return Response.status(400).entity(errorMsg).build();
        }
        String successMsg = "\"Your transfer is successfull\"";
        logger.info(successMsg);
        return Response.status(200).entity(successMsg).build();
    }
}
