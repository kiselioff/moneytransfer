package model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountPOJO {
    @JsonProperty
    private int clientID;
    @JsonProperty
    private double accountBalance;

    public AccountPOJO(int clientID, double accountBalance) {
        this.clientID = clientID;
        this.accountBalance = accountBalance;
    }

    public AccountPOJO() {
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }
}
